# Modules import
import calc_mass
import itertools
from scipy.spatial import distance


# Class definition
class Prediction:
    """
    This class is dedicated to the prediction of potential sequences that could match with a given pattern and a given
    mass.

    Attributes
    ----------
    pattern_freq : str
            A pattern representing a distribution of amino acids and their frequency.
    target_mass : float
        The targeted mass.
    markers_seqs : list(str)
        The list of marker peptides used to build the pattern ('pattern_freq').
    resolution : float
        The resolution defining the accepted imprecision in our results.

    Methods
    -------
    first_analyze():
        Generate a list of sequences from the pattern and test if their mass belong to the targeted mass interval.
    """

    def __init__(self, pattern_freq, target_mass, markers_seqs, resolution=0.1):
        """
        Initialization of the class Prediction.
        This part also permits to generate variables useful for subsequent analyses : an interval around the orphan
        targeted mass, a sequence composed of conserved aminoacids, a list allowing us to keep track of the order of the
        aminoacids in the sequences generated.

        Parameters
        ----------
        pattern_freq : str
            A pattern representing a distribution of amino acids and their frequency.
        target_mass : float
            The targeted mass.
        markers_seqs : list(str)
            The list of marker peptides used to build the pattern ('pattern_freq').
        resolution : float
            The resolution defining the accepted imprecision in our results.
        """
        self.__pattern_simplified, self.__frequencies_by_position = self.__parse_pattern(pattern_freq)
        self.__constant_seq, self.__base_to_build, self.__variable_pattern = self.__parse_pattern_simplified()
        self.__mass_interval = self.__interval(target_mass, resolution)
        # The extra_h is False and will be added manually later.
        self.__constant_mass = calc_mass.compute_peptide_mass_with_proline(self.__constant_seq, 0, False)
        self.__markers_variable_part = self.__extract_variable_parts_from_markers(markers_seqs)

    @staticmethod
    def __parse_pattern(pattern_freq):
        """
        Decomposes the pattern into two different structures: a character string corresponding to the pattern without
        the amino acid frequencies, and a list of dictionaries grouping the amino acid frequencies for each position.
        ex : pattern_freq == '1.00A-0.50A,0.25S,0.25T-1.00R'
             pattern_simplified == 'A-AST-R'
             frequencies_by_position == [{'A': 1.00}, {'A': 0.50, 'S': 0.25, 'T': 0.25}, {'R': 1.00}]

        Parameters
        ----------
        pattern_freq : str
            A pattern representing a distribution of amino acids and their frequency.

        Returns
        -------
        str :
            A character string corresponding to the pattern without the amino acid frequencies.
        list(dict(str:float)) :
            A list of dictionaries grouping the amino acid frequencies for each position.
        """
        pattern_simplified = ""
        frequencies_by_position = []
        if "-" in pattern_freq:
            pattern_split = pattern_freq.split("-")
        else:
            pattern_split = [pattern_freq]
        for i in range(len(pattern_split)):
            frequencies_by_position.append({})
            if "," in pattern_split[i]:
                aminoacids_split = pattern_split[i].split(",")
            else:
                aminoacids_split = [pattern_split[i]]
            for freq_aa in aminoacids_split:
                pattern_simplified += freq_aa[4:]
                frequencies_by_position[i][freq_aa[4:]] = float(freq_aa[:4])
            pattern_simplified += "-"
        return pattern_simplified.rstrip("-"), frequencies_by_position

    @staticmethod
    def __interval(num, resolution):
        """
        Calculate an interval around the value based on the resolution.

        Parameters
        ----------
        num : float
            The value to calculate an interval around.
        resolution : float
            Half size of the interval.

        Returns
        -------
        (int, int)
            The minimal and maximal value of the interval, respectively.
        """
        value1 = num - resolution
        value2 = num + resolution
        return value1, value2

    @staticmethod
    def __regroup_by_composition(list_seq):
        """
        To reduce the number the mass calculations, the variable sequences are grouped in a dictionary by their
        composition in aminoacids.

        Parameters
        ----------
        list_seq : list(str)
            A list of sequences.

        Returns
        -------
        dict(str:)):
            A dictionary storing all the sequences grouped by their composition in aminoacids.
        """
        sequences_grouped = {}
        for seq in list_seq:
            seq_ordered = "".join(sorted(list(seq)))
            if seq_ordered in sequences_grouped:
                sequences_grouped[seq_ordered].append(seq)
            else:
                sequences_grouped[seq_ordered] = [seq]
        return sequences_grouped

    def __calc_frequency(self, seq):
        """
        Calculates a frequency index of the amino acids of the sequence based on their presence in marker peptides.

        Parameters
        ----------
        seq : str
            A sequence generated from the pattern.

        Returns
        -------
        float :
            The frequency index.
        """
        freq = 1.0
        for i in range(len(seq)):
            freq *= self.__frequencies_by_position[i][seq[i]]
        return freq

    def __parse_pattern_simplified(self):
        """
        From the patter simplified, calculates :
        - a sequence composed of invariant amino acids.
        - a list keeping track of variable and non-variable positions to reconstruct the generated sequences.
        - a pattern made up of variable positions only.
        Example :
            pattern_simplified == 'A-AVS-RT-G'
            constant == 'AG'
            base == ['A', '', '', 'G']
            variable == 'AVS-RT'

        Returns
        -------
        str :
            The sequence composed of invariant amino acids.
        list(str) :
            A list keeping track of variable and non-variable positions to reconstruct the generated sequences.
        str :
            A pattern made up of variable positions only.
        """
        constant = ""
        variable = ""
        base = self.__pattern_simplified.split("-")
        for i in range(len(base)):
            if len(base[i]) == 1:
                constant += base[i]
            else:
                variable += base[i] + "-"
                base[i] = ''
        return constant, base, variable.rstrip("-")

    def __extract_variable_parts_from_markers(self, markers_seqs):
        """
        Extract amino acids from variable positions in marker peptides to keep track of "variable regions" in our data.

        Parameters
        ----------
        markers_seqs : list(str)
            The peptide markers list.

        Returns
        -------
        list(str) :
            The amino acids of the variable positions (ordered) of the peptide markers.
        """
        variable_parts = []
        variable_positions = []
        for i in range(len(self.__base_to_build)):
            if self.__base_to_build[i] == "":
                variable_positions.append(i)
        for seq in markers_seqs:
            part = ""
            for i in range(len(seq)):
                if i in variable_positions:
                    part += seq[i]
            variable_parts.append(part)
        return variable_parts

    def __test_mass_sequence(self, variable_seq):
        """
        Combine the mass of the constant sequence with the variable sequence with a range of number of hydroxylations
        and find which combination can correspond to our mass interval targeted.
        If the sequence with a number of hydroxylations match the interval, this function returns its mass with the
        corresponding number of hydroxylation. Else, it returns None.

        Parameters
        ----------
        variable_seq : str
            The variable part of an amino acids sequence.

        Returns
        -------
        None/(int, float):
            A number of hydroxylation of prolines associated with the corresponding peptide mass.
        """
        entire_peptide = self.__constant_seq + variable_seq
        nb_P = entire_peptide.count("P")
        min_P = int(nb_P * 0.2)
        max_P = int(nb_P * 0.8) + 1
        masses = calc_mass.compute_peptide_mass_with_proline_range(variable_seq, min_P, max_P, True)
        result = None
        for i in range(len(masses)):
            if variable_seq == "":
                mass = masses[i] + self.__constant_mass
            else:
                mass = masses[i] + self.__constant_mass - calc_mass.mass_H2O()
            if self.__mass_interval[0] < mass < self.__mass_interval[1]:
                result = (i + min_P, mass)
        return result

    def __generate_sequences_from_variable_pattern(self):
        """
        From the variable pattern, generate all possible sequences.

        Returns
        -------
        list(str) :
            A list of all sequences that can be generated from the variable pattern.
        """
        return ["".join(seq) for seq in itertools.product(*self.__variable_pattern.split("-"))]

    def __calc_hamming_distance(self, variable_seq):
        """
        Calculates the minimal hamming distance between a sequence generated from the variable pattern and the
        variable regions of the peptide markers.

        Parameters
        ----------
        variable_seq : str
            A sequence generated from the variable pattern.

        Returns
        -------
        int :
            The minimal hamming distance between the sequence and the variable regions of the peptide markers
        """
        return round(min(distance.hamming(list(variable_seq), list(markers_var_seq)) for markers_var_seq
                         in self.__markers_variable_part) * len(variable_seq))

    def __reconstruct(self, var_seq):
        """
        Build a peptide sequence from a construction base and a variable sequence of aminoacids in the order of
        construction with the variable pattern.

        Parameters
        ----------
        var_seq : str
            A sequence of aminoacids.

        Returns
        -------
        str :
            The reconstructed sequence.
        """
        base = self.__base_to_build[:]
        inserted = 0
        for i in range(len(base)):
            if base[i] == "":
                base[i] = var_seq[inserted]
                inserted += 1
        return "".join(base)

    def get_mass_interval(self):
        """
        Return the interval of mass accepted to valid a sequence as a potential marker.

        Returns
        -------
        (float, float):
            The interval of masses.
        """
        return self.__mass_interval

    def first_analyze(self):
        """
        Generate a list of sequences from the pattern and test if their mass belong to the targeted mass interval.

        Returns
        -------
        list((str, int, float, int, float)) :
            Information about the sequences matching with the mass interval.
            Respectively the peptide sequence, the minimal hamming distance, the frequency index associated, the number
            of hydroxylation and the mass.
        """
        results = []
        if len(self.__variable_pattern) == 0:  # When the pattern is constant, there is no variability in the sequence.
            combi = self.__test_mass_sequence("")
            if combi is not None:
                ptm = combi[0]
                mass = combi[1]
                hamming_distance = 0
                freq = 1.0
                results.append((self.__constant_seq, hamming_distance, freq, ptm, mass))
        else:
            variable_sequences_generated = self.__generate_sequences_from_variable_pattern()
            variable_sequences_grouped = self.__regroup_by_composition(variable_sequences_generated)
            for variable_seq in variable_sequences_grouped:
                combi = self.__test_mass_sequence(variable_seq)
                if combi is not None:
                    ptm = combi[0]
                    mass = combi[1]
                    for var_seq in variable_sequences_grouped[variable_seq]:
                        hamming_distance = self.__calc_hamming_distance(var_seq)
                        seq = self.__reconstruct(var_seq)
                        freq = self.__calc_frequency(seq)
                        results.append((seq, hamming_distance, freq, ptm, mass))
        return results

    def second_analyze(self):
        """
        >> Idea not developed <<
        If the first analyze doesn't return any result or any relevant result, the second analyze try to define some
        positions as non-conserved. In priority, these position are one with the lowest conservation score (defined in a
        variable named "conservation" in the class Pattern). For the positions, all aminoacids are tested instead of
        only those in the pattern.
        positions.

        Returns
        -------

        """
        pass

