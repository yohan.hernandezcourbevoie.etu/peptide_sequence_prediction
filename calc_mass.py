# Modules import
from pyteomics import mass


def mass_H2O():
    """
    Calculate the mass of a water molecule, corresponding to the mass lost when a peptide bond is formed.

    Returns
    -------
    float :
        The mass of a water molecule.
    """
    return mass.calculate_mass(formula='H2O')


def peptide_mass(peptide_seq):
    """
    Calculate the mass of a peptide without considering PTMs.

    was (with molmass) https://pypi.org/project/molmass/
    formula = Formula(seq_peptide)
    mass = formula.isotope.mass

    Parameters
    ----------
    peptide_seq : str
        An aminoacid sequence.

    Returns
    -------
    float :
        An estimated mass for the peptide.
    """
    return mass.calculate_mass(peptide_seq)


def compute_peptide_mass_with_proline(peptide_seq, number_of_prolines, extra_h):
    """
    Calculate the mass of a peptide considering a given number of hydroxyprolines.

    Parameters
    ----------
    peptide_seq : str
        An aminoacid sequence.
    number_of_prolines : int
        The number of hydroxylations of prolines to take into account in the calculation.
    extra_h : bool
        True if an additional hydrogen is to be considered.

    Returns
    -------
    float :
        An estimated mass for the peptide.
    """
    if 'Z' in peptide_seq or 'B' in peptide_seq or 'X' in peptide_seq:
        return 0
    current_mass = peptide_mass(peptide_seq)
    if extra_h:
        current_mass += 1
    current_mass += mass.calculate_mass(formula='O') * number_of_prolines
    return current_mass


def compute_peptide_mass_with_proline_range(peptide_seq, min_P, max_P, extra_h):
    """
    Calculate the mass of a peptide considering a range of number of hydroxyprolines.

    Parameters
    ----------
    peptide_seq : str
        An aminoacid sequence.
    min_P :
        The minimal number of hydroxylations of prolines to consider.
    max_P :
        The maximal number of hydroxylations of prolines to consider.
    extra_h : bool
        True if an additional hydrogen is to be considered.

    Returns
    -------
    list(int):
        A list of masses corresponding to the sequence varying with the number of hydroxylations.
    """
    if 'Z' in peptide_seq or 'B' in peptide_seq or 'X' in peptide_seq:
        return []
    current_mass = peptide_mass(peptide_seq)
    if extra_h:
        current_mass += 1
    if min_P > max_P:
        min_P = max_P
    current_mass += mass.calculate_mass(formula='O') * min_P
    mass_list = [current_mass]
    for i in range(min_P, max_P):
        current_mass += mass.calculate_mass(formula='O')
        mass_list.append(current_mass)
    return mass_list
