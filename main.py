"""
From a table of peptides markers, a mass and a marker name, this script returns different potential sequences matching
the criteria.

Example of command:
python3 main.py --search -t table_selected_species_mammals_col.tsv -m 1180.6360 -n A D
python3 main.py --pattern -t table_selected_species_mammals_col.tsv
"""

# Modules import
from markers import Markers
from pattern import Pattern
from prediction import Prediction
import argparse
import os


# Functions definition
def add_info_normal(dictio, seq, class1, class2, sort, info):
    if class1 in dictio:
        if class2 in dictio[class1]:
            dictio[class1][class2].append((seq, sort, info))
        else:
            dictio[class1][class2] = [(seq, sort, info)]
    else:
        dictio[class1] = {class2: [(seq, sort, info)]}


def add_info_prop(dictio, seq, class1, class2):
    seq = "".join(sorted(list(seq)))
    if class1 in dictio:
        if class2 in dictio[class1]:
            dictio[class1][class2].add(seq)
        else:
            dictio[class1][class2] = {seq}
    else:
        dictio[class1] = {class2: {seq}}


# Main program
if __name__ == "__main__":
    # Initialization of arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument("--table", "-t", help="Required. Path to a TSV file containing the peptide markers.",
                        action="store", type=str, default=None, required=True)
    parser.add_argument("--pattern", help="Create file with a pattern for every marker category.",
                        action="store_true")
    parser.add_argument("--search", help="Search a sequence corresponding to a given mass. \n"
                                         "With this option, --mass [FLOAT] and --name [STR] are required.",
                        action="store_true")
    parser.add_argument("--output", "-o", help="Output file core name.", action="store", type=str, default=None)
    parser.add_argument("--mass", "-m", help="Mass of the orphan peak.",
                        action="store", type=float, default=None, nargs="*")
    parser.add_argument("--name", "-n", help="Name of the missing marker.",
                        action="store", type=str, default=None, nargs="*")
    parser.add_argument("--resolution", "-r", help="The degree of imprecision accepted.",
                        action="store", type=float, default=0.1)
    parser.add_argument("--taxids", "-i", help="Optional. For both --pattern and --search options, it is possible to "
                                               "select a limited number of species to buidl the pattern(s) on. The "
                                               "identifier of species is their taxon id.",
                        action="store", type=str, default=None, nargs="*")
    args = parser.parse_args()

    # Table parsing
    assert os.path.exists(args.table), "The following file does not exist: {}".format(args.table)
    markers_object = Markers(args.table, taxids=args.taxids)

    # Option "pattern" : Create file with a pattern for every marker category.
    if args.pattern:
        all_patterns = {}
        for name in markers_object.get_names():
            pattern_object = Pattern(markers_object.get_sequences(name))
            pattern = pattern_object.get_pattern()
            snp_dict = pattern_object.get_substitutions_infos()
            all_patterns[name] = (pattern, snp_dict)

        # Output
        if not os.path.exists('./pattern'):
            os.makedirs('pattern')
        if args.output:
            output_file = "pattern/" + args.output + ".txt"
        else:
            output_file = "pattern/" + ".".join(args.table.split('/')[-1].split('.')[:-1]) + f"_pattern.txt"
        with open(output_file, "w") as fileOut:
            fileOut.write(f"Source file: {args.table.split('/')[-1]}\n")
            for name in all_patterns:
                fileOut.write("\n")
                fileOut.write(f"Name: {name}\n")
                fileOut.write(f"Pattern: {all_patterns[name][0]}\n")
                for imprecise in all_patterns[name][1]:
                    fileOut.write(f"{imprecise}\t")
                    for couple in all_patterns[name][1][imprecise]:
                        fileOut.write(f"{couple[0]}={couple[1]} ")
                    fileOut.write("\n")

    # Option "search" : Search a sequence corresponding to a given mass.
    if args.search:
        assert args.mass is not None and args.name is not None, \
            "The option --search require the options --mass and --name."
        for name_iter in args.name:
            # Pattern
            markers_seqs = markers_object.get_sequences(name_iter)
            pattern_object = Pattern(markers_seqs)
            pattern = pattern_object.get_pattern()
            pattern_freq = pattern_object.get_pattern_freq()
            for mass_iter in args.mass:
                # Matching peptide sequences
                prediction_object = Prediction(pattern_freq, mass_iter, markers_seqs, resolution=args.resolution)
                mass_interval = prediction_object.get_mass_interval()
                match = prediction_object.first_analyze()

                # Output
                if not os.path.exists('./search'):
                    os.makedirs('search')
                if args.output:
                    core_name = "search/" + args.output + f"_mass{mass_iter}_name{name_iter}_search"
                else:
                    core_name = "search/" + ".".join(args.table.split('/')[-1].split('.')[:-1]) + \
                                f"_mass{mass_iter}_name{name_iter}_search"
                txt_file = core_name + ".txt"

                match_bymass_normal = {}
                match_bymass_prop = {}
                for result in match:
                    seq = result[0]
                    hamming = result[1]
                    freq = result[2]
                    ptm = result[3]
                    mass = result[4]
                    add_info_normal(match_bymass_normal, seq, mass, ptm, freq, hamming)
                    add_info_prop(match_bymass_prop, seq, mass, ptm)
                space = "   "
                with open(txt_file, "w") as txtOut:
                    txtOut.write(f"Peptide marker: {name_iter},{space}Mass: {mass_iter},"
                                 f"{space}Mass interval: {mass_interval},{space}Number of matches: {len(match)}\n")
                    txtOut.write(f"Pattern: {pattern}\n\n")

                    txtOut.write("Sequences grouped by mass (sorted by frequency index):\n")
                    txtOut.write("------------------------------------------------------\n")
                    for mass in match_bymass_normal:
                        txtOut.write(f"Mass: {mass}\n")
                        for ptm in match_bymass_normal[mass]:
                            txtOut.write(f"{space}PTM: {ptm}\n")
                            for seq_freq in sorted(match_bymass_normal[mass][ptm], reverse=True, key=lambda x: x[1]):
                                txtOut.write(f"{space}{space}{seq_freq[0]}{space}{seq_freq[2]}{space}{seq_freq[1]}\n")
                    txtOut.write("\n")

                    txtOut.write("Sequences grouped by mass (proportions):\n")
                    txtOut.write("----------------------------------------\n")
                    for mass in match_bymass_prop:
                        txtOut.write(f"Mass: {mass}\n")
                        for ptm in match_bymass_prop[mass]:
                            txtOut.write(f"{space}PTM: {ptm}\n")
                            for seq in sorted(list(match_bymass_prop[mass][ptm])):
                                txtOut.write(f"{space}{space}{seq}\n")
