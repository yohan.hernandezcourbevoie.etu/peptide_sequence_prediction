# Modules import
import blosum


# Class definition
class Pattern:
    """
    This class is dedicated to the deduction of a pattern from a list of amino acid sequences.

    Attributes
    ----------
    sequences : list(str)
        A list of peptide sequences.

    Methods
    -------
    get_pattern() :
        Returns a pattern representing the distribution of amino acids in the peptide sequences.
    get_pattern_freq():
        Returns a pattern representing the distribution of amino acids in the peptide sequences and their frequency.
    get_substitutions_infos():
        Returns a dictionary storing the substitution scores of amino acids of the same position with each other when
        several aminoacids are present at the same position.
    get_conservation():
        Returns a string containing the lowest substitution score for each position of the peptide sequences.
    """

    def __init__(self, sequences):
        """
        Initializes an object of class Pattern.

        Parameters
        ----------
        sequences : list(str)
            A list of peptide sequences.
        """
        self.__MATRIX = blosum.BLOSUM(62)
        self.__sequences = self.__filter(sequences)
        self.__len = len(self.__sequences[0])
        self.__size = len(self.__sequences)
        self.__pattern, self.__pattern_freq = self.__calculate_pattern_patternfreq()
        self.__substitutions_dict = self.__analyze_substitutions()
        self.__conservation = self.__calculate_conservation()

    @staticmethod
    def __filter(sequences):
        """
        For the pattern to be built, all sequences must be of the same length. This method keeps the sequences with the
        most frequent length.

        Parameters
        ----------
        sequences : list(str)
            A list of peptide sequences.

        Returns
        -------
        list(str) :
            A list of peptide sequences of the same length.
        """
        length_dict = {}
        for seq in sequences:
            length = len(seq)
            if length in length_dict:
                length_dict[length] += 1
            else:
                length_dict[length] = 1
        maxi = 0
        keep_length = 0
        for length in length_dict:
            if length_dict[length] > maxi:
                maxi = length_dict[length]
                keep_length = length
        for seq in sequences:
            if len(seq) != keep_length:
                sequences.remove(seq)
        return sequences

    @staticmethod
    def __add_information_pattern(aminoacids):
        """
        Produce a string to add in the pattern with all aminoacids at a specific position.

        Parameters
        ----------
        aminoacids : list(str)
            A list containing the aminoacids to add in the pattern.

        Returns
        -------
        subpattern : str
            Subpattern of a specific position un the peptide sequences.
        """
        unique_aa = set(aminoacids)
        if len(unique_aa) == 1:
            subpattern = f"{''.join(unique_aa)}-"
        else:
            subpattern = f"[{''.join(unique_aa)}]-"
        return subpattern

    def __add_information_pattern_freq(self, aminoacids):
        """
        Produce a string to add in the pattern with all aminoacids their frequency at a specific position.

        Parameters
        ----------
        aminoacids : list(str)
            A set containing the aminoacids to add in the pattern.

        Returns
        -------
        subpattern : str
            Subpattern of a specific position un the peptide sequences.
        """
        freq = {}
        for aa in aminoacids:
            if aa in freq:
                freq[aa] += 1
            else:
                freq[aa] = 1
        subpattern = ""
        for aa in freq:
            subpattern += f"{freq[aa]/self.__size:.2f}{aa},"
        subpattern = subpattern.rstrip(",") + "-"
        return subpattern

    def __search_aminoacids_by_position(self, position):
        """
        Return a list of all amino-acids at a given position in the peptide sequence.

        Parameters
        ----------
        position : int
            The position to study.

        Returns
        -------
        list(str):
            All amino-acids at a given position.
        """
        aminoacids = []
        for i in range(self.__size):
            aminoacids.append(self.__sequences[i][position])
        return aminoacids

    def __calculate_pattern_patternfreq(self):
        """
        Return two patterns from a list of sequences of the same size :
            - 'pattern' represents the distribution of amino acids in the sequences.
            - 'pattern_freq' provides the same information as 'pattern' and adds the amino acid frequency.

        Returns
        -------
        str :
            A pattern representing the distribution of amino acids in the sequences.
        str :
            A pattern representing the distribution of amino acids in the sequence and their frequency.
        """
        pattern = ""
        pattern_freq = ""
        for i in range(self.__len):
            aminoacids = self.__search_aminoacids_by_position(i)
            pattern += self.__add_information_pattern(aminoacids)
            pattern_freq += self.__add_information_pattern_freq(aminoacids)
        return pattern.rstrip("-"), pattern_freq.rstrip("-")

    def __analyze_substitutions(self):
        """
        Based on the Blosum 62 matrix, this function provides, for each position in the pattern where several amino
        acids are present, the substitution score for amino acids in the same position.

        Returns
        -------
        dict(str:list(int, int)) :
            A dictionary storing the substitution scores of amino acids of the same position with each other, with the
            key being the amino acids of the same position in the pattern.
            ex : pattern == 'A-[AVS]-I'
                 substitutions_dict == {'AVS': [('AV', 0), ('AS', 1), ('VS', -2)]}
        """
        substitutions_dict = {}
        for aa_by_position in self.__pattern.split("-"):
            if "[" in aa_by_position:
                couples = []
                aminoacids = aa_by_position[1:-1]
                for i1 in range(len(aminoacids)):
                    for i2 in range(i1 + 1, len(aminoacids)):
                        couples.append((aminoacids[i1]+aminoacids[i2],
                                        int(self.__MATRIX[aminoacids[i1]][aminoacids[i2]])))
                substitutions_dict[aa_by_position] = couples
        return substitutions_dict

    def __calculate_conservation(self):
        """
        Creates a string where each position corresponds to the lowest substitution score between all amino acids of the
        same position in the previously created pattern.
        ex : pattern == 'A-[AVS]-I'
             conservation = '4;-2;4'

        Returns
        -------
        str :
            A string where each position corresponds to the lowest substitution score between all amino acids of the
            same position.
        """
        conservation = ""
        for aminoacids in self.__pattern.split("-"):
            if len(aminoacids) == 1:
                conservation += f"{int(self.__MATRIX[aminoacids][aminoacids])};"
            else:
                conservation += f"{min(self.__substitutions_dict[aminoacids], key=lambda t: t[1])[1]};"
        return conservation.rstrip(";")

    def get_pattern(self):
        """
        Returns a pattern representing the distribution of amino acids in the peptide sequences.

        Returns
        -------
        str :
            The pattern.
        """
        return self.__pattern

    def get_pattern_freq(self):
        """
        Returns a pattern representing the distribution of amino acids in the peptide sequences and their frequency.

        Returns
        -------
        str :
            The pattern.
        """
        return self.__pattern_freq

    def get_substitutions_infos(self):
        """
        Returns a dictionary storing the substitution scores of amino acids of the same position with each other when
        several aminoacids are present at the same position.

        Returns
        -------
        dict(str:list(int, int)) :
            A dictionary storing the substitution scores of amino acids of the same position with each other, with the
            key being the amino acids of the same position in the pattern.
        """
        return self.__substitutions_dict

    def get_conservation(self):
        """
        Returns a string containing the lowest substitution score for each position of the peptide sequences.

        Returns
        -------
        str :
            A string where each position corresponds to the lowest substitution score between all amino acids of the
            same position.
        """
        return self.__conservation
