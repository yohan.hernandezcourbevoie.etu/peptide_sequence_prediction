# Class definition
class Markers:
    """
    This class processes peptide marker tables in TSV files.

    Attributes
    ----------
    file : str
        Path to a TSV file containing a peptide markers table.

    Methods
    -------
    add_name(new_name)
        Add a new marker's name to the predefined list.
    delete_name(name_to_delete)
        Delete a marker's name.
    get_names()
        Gives the names making the different categories of markers.
    get_size()
        Returns the number of peptide markers associated with each name.
    get_sequences(name)
        Return all the peptides markers associated with a given name.
    """

    def __init__(self, file, taxids=None):
        """
        Initialize the class with the path to a TSV file.
        Is the 'taxids' parameter is set, only sequences of the requested species are processed.

        Parameters
        ----------
        file : str
            Path to a TSV file containing a peptide markers table.
        taxids : list(str)
            A list of taxid whose sequences are to be processed.
        """
        self.__NAMES = ["A", "B", "C", "D", "D", "E", "F", "G", "P1", "P2"]
        self.__markers = self.__table_parser(file, taxids)

    def __table_parser(self, file, taxids):
        """
        Parse the TSV file to extract the sequences for each marker name.

        Parameters
        ----------
        file : str
            The path to a TSV file containing a peptide markers table.
        taxids : list(str)
            A list of taxid whose sequences are to be processed.

        Returns
        -------
        dict(str:list(str)):
            A dictionary, classified by name, regrouping the different peptide markers.
        """
        markers = {}
        for name in self.__NAMES:
            markers[name] = []
        with open(file, "r") as fileIn:
            line = fileIn.readline()
            while line != "":
                infos = line.split("\t")
                if infos[5] in markers and infos[3] != "" and (taxids is None or infos[1] in taxids):
                    markers[infos[5]].append(infos[3])
                line = fileIn.readline()
        return markers

    def add_name(self, new_name):
        """
        Add a new marker's name to the predefined list.

        Parameters
        ----------
        new_name : str
            Marker's name to add.

        Returns
        -------
        None
        """
        self.__NAMES.append(new_name)

    def delete_name(self, name_to_delete):
        """
        Delete a marker's name.

        Parameters
        ----------
        name_to_delete : str
            The marker's name to delete.

        Returns
        -------
        None
        """
        self.__NAMES.remove(name_to_delete)

    def get_names(self):
        """
        Gives the names making the marker's names.

        Returns
        -------
        list(str):
            The names of the peptide markers.
        """
        return self.__NAMES

    def get_size(self):
        """
        Returns the number of peptide markers associated with each name.

        Returns
        -------
        dict(str:int)
        """
        size = {}
        for name in self.__NAMES:
            size[name] = len(self.__markers[name])
        return size

    def get_sequences(self, name):
        """
        Return all the peptides markers associated with a given name.

        Parameters
        ----------
        name : str
            One of the names making the different categories of markers.

        Returns
        -------
        list(str):
            A list of peptides markers.
        """
        return self.__markers[name]
